
public class Emplacement {
	
	private double largeur;
	private boolean disponible;
	private Oeuvre lOeuvre;
	
	public Emplacement (double plargeur,boolean pdisponible,Oeuvre plOeuvre) 
	{
		this.largeur = plargeur;
		this.disponible = pdisponible;
		this.lOeuvre = plOeuvre;
		
	}
	
	public Oeuvre getlOeuvre() {
		return lOeuvre;
	}


	public void setlOeuvre(Oeuvre lOeuvre) {
		this.lOeuvre = lOeuvre;
	}


	public double getLargeur() {
		return largeur;
	}
	
	
	public boolean getDisponible() {
		return disponible;
	}
	public void setDisponible(boolean disponible) {
		this.disponible = disponible;
	}
	

}
