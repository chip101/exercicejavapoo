
public class Oeuvre {
	
	private int num;
	private static int dernierNum =0;
	private int annee;
	private String artiste;
	private String titre;
	
	
	public String getTitre() {
		return titre;
	}
	public void setTitre(String titre) {
		this.titre = titre;
	}
	public int getNum() {
		return num;
	}
	public void setNum(int num) {
		this.num = num;
	}
	public int getAnnee() {
		return annee;
	}
	public void setAnnee(int annee) {
		this.annee = annee;
	}
	public String getArtiste() {
		return artiste;
	}
	public void setArtiste(String artiste) {
		this.artiste = artiste;
	}
	
	public Oeuvre (String partiste, int pannee,String ptitre) 
	{
		this.artiste = partiste;
		this.annee = pannee;
		this.titre = ptitre;
		this.num = dernierNum +1;
		dernierNum ++;
		
		
	}
	public void afficherCarac () 
	{
		System.out.println("Le num�ro de l'oeuvre est : "+ this.num);
		System.out.println("Le titre de l'oeuvre est : "+ this.titre);
		System.out.println("L'ann�e de l'oeuvre est : "+ this.annee);
		System.out.println("L'artiste de l'oeuvre est : "+ this.artiste);
		
	}

}
