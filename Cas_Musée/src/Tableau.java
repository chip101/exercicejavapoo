
public class Tableau extends Oeuvre{
	
	private int largeur;
	private int longueur;
	
	public Tableau (int plargeur,int plongueur, String partiste, int pannee,String ptitre)
	{
		super(partiste,pannee,ptitre);
		this.largeur = plargeur;
		this.longueur = plongueur;
	}

	public int getLargeur() {
		return largeur;
	}

	public void setLargeur(int largeur) {
		this.largeur = largeur;
	}

	public int getLongueur() {
		return longueur;
	}

	public void setLongueur(int longueur) {
		this.longueur = longueur;
	}
	
	public void afficherCarac () 
	{
		super.afficherCarac();
		System.out.println("Le tableau a une largeur de : "+this.largeur);
		System.out.println("Le tableau a une longueur de : "+this.longueur);
		
	}
}
