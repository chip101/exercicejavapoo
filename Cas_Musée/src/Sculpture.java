
public class Sculpture extends Oeuvre{
	
	private String materiau;
	
	public Sculpture (String partiste, int pannee,String ptitre, String pmateriau) 
	{
		super( partiste, pannee, ptitre);
		this.materiau = pmateriau;
	}

	public String getMateriau() {
		return materiau;
	}

	public void setMateriau(String materiau) {
		this.materiau = materiau;
	}
	
	public void afficherCarac () 
	{
		super.afficherCarac();
		System.out.println("Le materiau est "+this.materiau);
	}
}
