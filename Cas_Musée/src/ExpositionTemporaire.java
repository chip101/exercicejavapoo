import java.util.ArrayList;
import java.util.Date;
public class ExpositionTemporaire {
	
	private final static int MAX =60;
	
	private String nom;
	private int anPeriodeDebut;
	private int anPeriodeFin;
	private int nbVisite;
	
	private Date dateDebut;
	private Date dateFin;
	
	
	private Emplacement LesEmplacements[];
	
	private static ArrayList <Oeuvre> lesOeuvres =new ArrayList<Oeuvre>();
	
	
	public static ArrayList<Oeuvre> getLesOeuvres() {
		return lesOeuvres;
	}
	public static void setLesOeuvres(ArrayList<Oeuvre> lesOeuvres) {
		ExpositionTemporaire.lesOeuvres = lesOeuvres;
	}
	
	
	public int getNbVisite() {
		return nbVisite;
	}
	public void setNbVisite(int nbVisite) {
		this.nbVisite = nbVisite;
	}
	
	
	
	public ExpositionTemporaire (String pnom, int panPeriodeDebut,int panPeriodeFin, int pnbVisite, Date pdateDebut, Date pdateFin)
	{
		this.nom = pnom;
		this.nbVisite =pnbVisite;
		this.anPeriodeDebut = panPeriodeDebut;
		this.anPeriodeFin = panPeriodeFin;
		this.dateDebut = pdateDebut;
		this.dateFin = pdateFin;
		
		this.lesOeuvres = PlanificationMusee.getOeuvresPeriode( panPeriodeDebut,panPeriodeFin);
		this.initEmplacement();
		this.nbVisite=0;
	} 
	
	public void initEmplacement() 
	{
		this.LesEmplacements = new Emplacement [MAX];
	}
	
	public void affecterOeuvre (Tableau punTableau) 
	{
		int i = 0;
		boolean trouve = false;
		
		while (i<MAX && trouve == false) 
		{
			if (LesEmplacements[i].getLargeur() == punTableau.getLargeur()) 
			{
				if (LesEmplacements[i].getDisponible()== true) 
				{
					LesEmplacements[i].setlOeuvre(punTableau);
					LesEmplacements[i].setDisponible(false);
					 trouve = true;
				}
			}
		}
		if (trouve == false) 
		{
			System.out.println("Il n'y a plus de places a la longueurs disponible");
		}
		
		
	}
	
	
}
