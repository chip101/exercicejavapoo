import java.util.ArrayList;

public class PlanificationMusee {
	
	private static ArrayList <Oeuvre> lesOeuvres =new ArrayList<Oeuvre>();
	private static ArrayList <ExpositionTemporaire> lesExpos = new ArrayList<ExpositionTemporaire>();

	
	
	
	public ArrayList<ExpositionTemporaire> getLesExpos() {
		return lesExpos;
	}

	public void setLesExpos(ArrayList<ExpositionTemporaire> lesExpos) {
		this.lesExpos = lesExpos;
	}

	public ArrayList<Oeuvre> getLesOeuvres() {
		return lesOeuvres;
	}

	public void setLesOeuvres(ArrayList<Oeuvre> plesOeuvres) {
		lesOeuvres = plesOeuvres;
	} 
	
	
	
	
	
	
	public static ArrayList <Oeuvre> getOeuvresPeriode (int andeb, int anfin)
	
	{
		ArrayList <Oeuvre> collectionOeuvres = new ArrayList <Oeuvre>();
		for (Oeuvre uneOeuvre : lesOeuvres) 
		{
			if (uneOeuvre.getAnnee()<= anfin && uneOeuvre.getAnnee()>=andeb) 
			{
				collectionOeuvres.add(uneOeuvre);
			}
		}
		return collectionOeuvres;
	}
	
	public ExpositionTemporaire expoLaPlusVisitee() 
	{
		ExpositionTemporaire expoMax = lesExpos.get(0);
		for (int i =0; i <this.lesExpos.size();i++) 
		{
			if (lesExpos.get(i).getNbVisite() > expoMax.getNbVisite()) 
			{
				expoMax = lesExpos.get(i);
			}
		}
		return expoMax;
	}
}
