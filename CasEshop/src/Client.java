import java.util.ArrayList;

public class Client {
	
	private static int numClient =0;
	private String nom;
	private String prenom;
	private String adresse;
	private String numTel;
	private String adresseMail;

	private ArrayList <Commande> lesCommandes = new ArrayList <Commande>();
	
	
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public String getAdresse() {
		return adresse;
	}
	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}
	public String getNumTel() {
		return numTel;
	}
	public void setNumTel(String numTel) {
		this.numTel = numTel;
	}
	public String getAdresseMail() {
		return adresseMail;
	}
	public void setAdresseMail(String adresseMail) {
		this.adresseMail = adresseMail;
	}
	public static int getNumClient() {
		return numClient;
	}
	
	public Client(String pnom, String pprenom, String padresse, String pnumtel, String padresseMail, ArrayList plesCommandes) 
	{
		this.numClient += 1;
		numClient +=1;
		
		this.nom = pnom;
		this.prenom = pprenom;
		this.adresse = padresse;
		this.numTel =pnumtel;
		this.adresseMail = padresseMail;
		this.lesCommandes = plesCommandes;
		
	}
	
	public void afficher () 
	{
		System.out.println("Voici respectivement le numero client , nom , prenom , adresse , numero de telephone et l adresse mail");
		System.out.println(this.numClient + "," + this.nom + "," + this.prenom  + "," + this.adresse + "," + this.numTel +"," + this.adresseMail);
	}
	
}
